from iterclass import I

#Example of iterable classes in use
if __name__ == '__main__':
    #Create iterable class using I
    class Creature(I):
        
        def __init__(self, name):
            #Must call super() for registry functions `__init__` and `delete` if they are overwritten
            super(Creature,self).__init__()
            self.name = name
        
        #Creatures are represented by their names.
        def __repr__(self):
            return self.name 
        
        #Easily compare creature against names or other creatures.
        def __eq__(self,other):
            if type(other) == str:
                return self.name == other
            else:
                return object.__repr__(self) == object.__repr__(other)

        def __del__(self):
            print('%s freed from memory' % self)
            
        def greet(self):
            print('Errrg %s' % self)
        
        #Must call super function if overwritten   
        def delete(self):
            print('%s died' % self)
            super(Creature,self).delete()
            
    class Person(Creature):
        
        def greet(self):
            print('Hi, I\'m %s'%self.name)
         
    class Tiger(Creature):
        
        def greet(self):
            print('Rawr, I\'m %s'%self.name)
    
    class Nation(I):
        def __init__(self, name):
            #Must call super() functions if they are overwritten
            super(Nation,self).__init__()
            self.name = name
        def __repr__(self):
            return self.name
    #Can dynamically create instances in batches without
    #explicitly storing them outside the registry
    for name in ('Jeff','Doc','Joe','Carole'):
        Person(name)
        
    for name in ('Fireball','Pearl','Enzo','Mo','Curly'):
        Tiger(name)
        
    Nation('the US of A')
    
    #len(class) returns # of instances
    print('Once upon a time there were %s creatures in %s' % (len(Creature),Nation[0]))
    
    #Can iterate thru the class for individual instances
    for person in Person:
        person.greet()
        if person == 'Joe':
            print('    And I have %s tigers:'%(len(Tiger)))
            names = '    '
            #I like for loops more lmao
            tiger_iter = iter(Tiger)
            while True:
                try:
                    names += next(tiger_iter).name + ' '
                except StopIteration:
                    del tiger_iter
                    break
            print(names)
    #Delete iteration var. It stores the last item in list and prevents it
    #from being deleted and freed from memory.
    del person
    
    #Can use list methods like index with classes.
    n = Person.index('Carole')
    victim = Person[n].name
    Person[n].delete()

    print('The %s of us are happy now without that bitch %s'%(len(Person),victim))
    print('Now we can keep our %s tigers'%len(Tiger))

    print('OH NO SOMEONE GOT A 25 KILL STREAK! TACTICAL NUKE INCOMING!!!')
    Creature.clear()
    print('Now there are %s creatures, %s people, and %s tigers' % (len(Creature),len(Person),len(Tiger)))
