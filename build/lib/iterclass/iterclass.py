class Iterclass(type):
    """
    Metaclass which create classes that iterate their instances
    
    Classes created by this metaclass may be treated as lists
    containing their instances, sharing most methods with lists.
    These classes are iterable and subscriptable. Any overwritten
    methods such as `__init__` or `delete` must call the parent
    method via `super()`.
    
    Usage:
        
    Class Cls(metaclass=IterClass):
        def __init__(self,*args,**kwargs):
            super().__init__()
            foo
    """
    class Registry:
        """
        Parent class of any class created by Iterclass.
        
        Provides methods to add and remove objects from
        the registry list.
        """
        def __init__(self):
            """Add self to list of instances"""
            self._registry.append(self)
            #Add self to parents' list of instances
            bases = type(self).__bases__
            for base in bases:
                if issubclass(base,Iterclass.Registry) and base not in (Iterclass.Registry,I):
                    base._registry.append(self)
        
        def delete(self):
            """
            Deletes instances.
            
            If another reference exists, instance will remain.
            It will be gone from the registry however, and
            functionally deleted when iterating.
            """
            #Remove self from list of instances
            self._registry.remove(self)
            #Remove self from parents' list of instances
            for base in type(self).__bases__:
                if issubclass(base,Iterclass.Registry) and base not in (Iterclass.Registry,I):
                    base._registry.remove(self)
            #Remove self from childen's list of instances
            for sub in type(self).__subclasses__():
                sub._registry.remove(self)
    
    def __new__(cls, name, bases, attrs):
        """
        Create class with parent Registry and 2 predefined vars
        """
        #Avoid incest (Method Resolution Order problems)
        addparent = True
        for base in bases:
            if issubclass(base,Iterclass.Registry):
                addparent = False
        if addparent:
            bases = (Iterclass.Registry,) + bases
        #I doesn't need the extra vars
        if name != 'I':
            attrs.update({'_i':0,'_registry':[]})
        return type.__new__(cls, name, bases, attrs)
    
    def __next__(cls):
        """
        Allows iteration through `next(cls)`
        """
        if cls._i >= len(cls._registry):
            cls._i = 0
            raise StopIteration
        _i = cls._i
        cls._i += 1
        return cls._registry[_i]

    def clear(cls):
        """
        Clears registry of self and children
        """
        cls._registry = []
        for sub in cls.__subclasses__():
            sub._registry = []

    def pop(cls,pos):
        """
        Remove and return the element at the specified position from the registry
        """
        #Remove item from list of instances
        item = cls._registry.pop(pos)
        #Remove self from parents' list of instances
        for base in type(item).__bases__:
            if issubclass(base,Iterclass.Registry) and base not in (Iterclass.Registry,I):
                base._registry.remove(item)
        #Remove self from childen's list of instances
        for sub in type(item).__subclasses__():
            sub._registry.remove(item)

        return item

    def remove(cls,item):
        """Remove item from registry"""
        #Remove self from list of instances
        cls._registry.remove(item)
        #Remove self from parents' list of instances
        for base in cls.__bases__:
            if issubclass(base,Iterclass.Registry) and base not in (Iterclass.Registry,I):
                base._registry.remove(item)
        #Remove self from childen's list of instances
        for sub in cls.__subclasses__():
            sub._registry.remove(item)

    #Implement list methods dynamically except blocklist

    blocklist = ('__repr__','__hash__','__getattribute__','__lt__','__le__','__eq__','__ne__',\
        '__gt__','__ge__','__init__','__iadd__','__imul__','__new__','__doc__','clear','pop','remove')

    for method in list.__dict__:
        if method not in blocklist:
            exec('def %s(cls,*args,**kwargs):\n'
                '    """Dynamically defined list object method.\n'  
                '    %s"""\n'
                '    return cls._registry.%s(*args,**kwargs)'%(method,list.__dict__[method].__doc__,method))


#class I(metaclass=Iterclass):
I = Iterclass('I',(object,),{})

I.__doc__ = """
    Allows shorthand for making iterable classes
    using regular inheritance.
    
    These classes behave identically to those created
    directly by Iterclass. Any overwritten methods
    such as `__init__` or `delete` must call their
    parent function via `super(Cls,self)`
    
    Usage:
        
    class Cls(I):
        def __init__(self,*args,**kwargs):
            super(Cls,self).__init__()
            foo
    """