import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    
    name="iterclass-trainraider7",
    version="1",
    author="Robert Rapier",
    author_email="xtrumpeterx@yahoo.com",
    description="Creates classes that can iterate their instances",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/trainraider7/iterclass",
    packages=setuptools.find_packages(),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Programming Language :: Cython",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)