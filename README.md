# iterclass

This python project provides a single metaclass that creates classes that can iterate over their instances as if the class itself were a list of its instances.